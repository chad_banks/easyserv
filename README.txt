EasyServ Quick Start Steps

1. Simply open the EasyServ.jar file by double clicking it. [1]
2. Select a save slot by clicking on it.
3. Customize the server motd, seed, and etc. (optional)
4. Click start.
5. Once the server startup process is compelted, you will be able to connect and play.
6. You can connect to the server from any computer on the same network by using the EasyServ computers IP address. [2]
7. To connect to servers from outside of your network, you will need to setup port forwarding with your router. [3]
8. Definitely provide feedback to help this grow. I would love to hear how EasyServ could be better! [4]

[1] If Java is installed correctly and updated, it will run the file.

[2] To find your computers IP.
http://lmgtfy.com/?q=how+to+find+my+computer%27s+IP

[3] To setup port forwarding.
http://lmgtfy.com/?q=how+to+setup+port+forwarding

[4] You can contact me here.
http://www.chadrbanks.com/contact

You may be able to find more information about EasyServ here.
http://www.chadrbanks.com/easyserv


- Chad R. Banks