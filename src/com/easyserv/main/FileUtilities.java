package com.easyserv.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileUtilities
{
	/**
	* FileUtilities
	* File utilities for to recursively copy directories
	*
	* @author  Chad R. Banks
	* @release 3
	* @since   2015-08-18
	*/
	private FileUtilities() {}
  
	public static final void copy( File source, File destination ) throws IOException
	{
		if( source.isDirectory() )
		{
			copyDirectory( source, destination );
		}
		else
		{
			copyFile( source, destination );
		}
	}
	  
	public static final void copyDirectory( File source, File destination ) throws IOException
	{
		if( !source.isDirectory() )
		{
			throw new IllegalArgumentException( "Source (" + source.getPath() + ") must be a directory." );
		}
		
		if( !source.exists() )
		{
			throw new IllegalArgumentException( "Source directory (" + source.getPath() + ") doesn't exist." );
		}
		
		if( destination.exists() )
		{
			throw new IllegalArgumentException( "Destination (" + destination.getPath() + ") exists." );
		}
		    
		destination.mkdirs();
		File[] files = source.listFiles();
		    
		for( File file : files )
		{
			if( file.isDirectory() )
			{
				copyDirectory( file, new File( destination, file.getName() ) );
			}
			else
			{
				copyFile( file, new File( destination, file.getName() ) );
			}
		}
	}
  
	public static final void copyFile( File source, File destination ) throws IOException
	{
		FileInputStream fis = new FileInputStream( source );
		FileOutputStream fos = new FileOutputStream( destination );
		FileChannel sourceChannel = fis.getChannel();
		FileChannel targetChannel = fos.getChannel();
		sourceChannel.transferTo(0, sourceChannel.size(), targetChannel);
		sourceChannel.close();
		targetChannel.close();
		fis.close();
		fos.close();
	}
}