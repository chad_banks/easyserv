package com.easyserv.main;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

public class Engine extends Canvas implements Runnable
{
	/**
	* Engine
	* Main EasyServ Engine Class
	*
	* @author  Chad R. Banks
	* @release 3
	* @since   2015-08-18
	* @thanks  RealTutsGML
	*/
	private static final long serialVersionUID = 6171590574899414329L;

	public static void main( String[] args )
	{
		new Engine( );
	}
	
	public static final int WIDTH = 800, HEIGHT = WIDTH / 12 * 9;
	protected StateScreen screenState = StateScreen.Main;
	private boolean running = false;
	public static int release = 3;
	private handlerMouse mh;
	public screenMain sMain;
	public screenSlot sSlot;
	private Thread thread;
	public int slot = 0;
	
	public Engine( )
	{
		sMain = new screenMain( this );
		sSlot = new screenSlot( this );
		
		mh = new handlerMouse( this );
		this.addMouseListener( mh );
		
		new Window( WIDTH, HEIGHT, "EasyServ", this );
	}

	public synchronized void start( )
	{
		thread = new Thread( this );
		thread.start( );
		running = true;
	}

	public synchronized void stop( )
	{
		try
		{
			thread.join( );
			running = false;
		}
		catch(Exception e)
		{
			e.printStackTrace( );
		}
	}

	@Override
	public void run( )
	{
		this.requestFocus( );
		long lastTime = System.nanoTime( );
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis( );

		while(running)
		{
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while(delta >= 1)
			{
				tick( );
				delta--;
			}
			if( running )
				render( );
			
			if( System.currentTimeMillis( ) - timer > 1000 )
			{
				timer += 1000;
			}
		}
		
		stop();
	}
	
	private void render( )
	{
		BufferStrategy bs = this.getBufferStrategy( );
		if( bs == null )
		{
			this.createBufferStrategy( 3 );
			return;
		}
		
		Graphics g = bs.getDrawGraphics( );
		
		g.setColor(Color.black);
		g.fillRect( 0, 0, WIDTH, HEIGHT );
		
		g.setFont( new Font("Arial", 1, 10 ) );
		g.setColor(Color.red);
		g.drawString( "r" + release, 10, 560 );
		
		if( screenState == StateScreen.Main )
		{
			sMain.render(g);
		}
		else if( screenState == StateScreen.Slot )
		{
			sSlot.render(g);
		}
		
		g.dispose( );
		bs.show( );
	}
	
	private void tick( )
	{
		if( screenState == StateScreen.Main )
		{
			sMain.tick( );
		}
		else if( screenState == StateScreen.Slot )
		{
			sSlot.tick ();
		}
	}
}