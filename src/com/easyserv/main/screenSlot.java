package com.easyserv.main;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class screenSlot
{
	/**
	* screenSlot
	* EasyServ Server Slot Screen (View)
	*
	* @author  Chad R. Banks
	* @release 3
	* @since   2015-08-18
	*/
	Server s;
	private Engine eng;
	private boolean running = false;

	static int bCol = 50, bRow = 50, bl = 160, bh = 32;
	static int sCol = 60, sRow = 50;

	private String[] tx = new String[20];
	private Color[] co = new Color[20];
	
	String dirn;
	int slot;
	
	public screenSlot( Engine e )
	{
		eng = e;
		clear( );
		setColor();
	}
	
	public static boolean mouseOver( int mx, int my, int x, int y, int w, int h )
	{
		if( mx > x && mx < x+w )
		{
			if( my > y && my < y+h )
			{
				return true;
			}
		}
		return false;
	}
	
	public void start( int sl )
	{
		s = new Server( this, sl );
		dirn = "srv" + sl;
		slot = sl;

		eng.screenState = StateScreen.Slot;
	}
	
	public void clear( )
	{
		for( int i = 0; i < 20; i++ )
		{
			tx[i] = "";
		}
	}
	
	private void setColor( )
	{
		for( int i = 0; i < 20; i++ )
		{
			co[i] = Color.red;
		}
	}
	
	public void out( String s )
	{
		for( int i = tx.length; i > 1; i-- )
		{
			tx[i - 1] = tx[i - 2];
			co[i - 1] = co[i - 2];
		}

		tx[0] = s;
		co[0] = Color.white;
	}
	
	public void out( String s, Color c )
	{
		for( int i = tx.length; i > 1; i-- )
		{
			tx[i - 1] = tx[i - 2];
			co[i - 1] = co[i - 2];
		}

		tx[0] = s;
		co[0] = c;
	}

	public void onClick( int mx, int my )
	{
		if(mouseOver(mx, my, bCol*1, bRow*1, bl, bh))// 1 - Start/Stop
		{
			if( running )
			{
				//s.stop( );
				//running = false;
			}
			else
			{
				clear( );
				s.start( );
				running = true;
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*2, bl, bh))// 2
		{
			if( running )
			{
				s.comm( "save-all" );
			}
			else
			{
				s.updateProperty( "motd" );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*3, bl, bh))// 3
		{
			if( running )
			{
				String userInput = JOptionPane.showInputDialog( "Who would you like to op?" );
				
				if( userInput != null )
				{
					s.op( userInput );
				}
			}
			else
			{
				s.updateProperty( "level-seed" );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*4, bl, bh))// 4
		{
			if( running )
			{
				String userInput = JOptionPane.showInputDialog( "Who would you like to de-op?" );
				
				if( userInput != null )
				{
					s.deop( userInput );
				}
			}
			else
			{
				s.toggleProperty( "hardcore" );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*5, bl, bh))// 5
		{
			if( running )
			{
				s.comm( "list" );
			}
			else
			{
				s.toggleProperty( "online-mode" );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*6, bl, bh))// 6
		{
			if( running )
			{
				JPanel panel = new JPanel( );
                panel.add(new JLabel("Choose a help page:"));
                DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
                model.addElement("1");
                model.addElement("2");
                model.addElement("3");
                model.addElement("4");
                model.addElement("5");
                model.addElement("6");
                model.addElement("7");
                model.addElement("8");
                model.addElement("9");
                JComboBox<String> comboBox = new JComboBox<String>(model);
                panel.add(comboBox);
                
                int result = JOptionPane.showConfirmDialog(null, panel, "Help Page Selection", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                String page = (String) comboBox.getSelectedItem( );
                
                if( result == 0 )
                {
                	s.comm( "help " + page );
                }
			}
			else
			{
				s.toggleProperty( "pvp" );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*7, bl, bh))// 7
		{
			if( running )
			{
				//
			}
			else
			{
				s.updateProperty( "server-port" );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*8, bl, bh))// 8 - Wipe World Content!
		{
			if( running )
			{
				String userInput = JOptionPane.showInputDialog( "Please input your desired command." );
				
				if( userInput != null )
				{
					s.comm( userInput );
				}
			}
			else
			{
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to delete this servers world?","Delete World", dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION)
				{
					File dir = new File( dirn + "/World" );
					s.purgeDirectory( dir );
					JOptionPane.showMessageDialog( null, "Your servers world has been deleted!" );
				}
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*9, bl, bh))// 9 - Visit Site
		{
			String site = "http://www.chadrbanks.com/easyserv";
			if( Desktop.isDesktopSupported( ) )
			{
				  try
				  {
					  Desktop.getDesktop( ).browse( new URI( site ) );
				  }
				  catch( IOException | URISyntaxException e1 )
				  {
					  out( "Sorry, looks like I cannot open the link to chadrbanks.com" );
					  e1.printStackTrace( );
				  }
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*10, bl, bh))// 10 - Quit
		{
			if( running )
			{
				s.stop( );
				running = false;
				//eng.screenState = StateScreen.Main;
			}
			else
			{
				eng.screenState = StateScreen.Main;
			}
		}
	}
	
	public void render( Graphics g )
	{
		Font fnt = new Font( "Arial", 1, 25 );
		Font fnt2 = new Font( "Arial", 1, 15 );
		Font fnt3 = new Font( "Arial", 1, 10 );
		
		g.setFont( fnt );
		g.setColor(Color.red);
		g.drawString( "EasyServ Slot " + slot, 20, 30 );

		g.setFont( fnt2 );
		if( !running )
		{
			g.drawRect(bCol, bRow*1, bl, bh);
			g.drawString("Start Server", sCol, 70);
			
			g.drawRect(bCol, bRow*2, bl, bh);
			g.drawString("Edit Server MOTD", sCol, 120);
			
			g.drawRect(bCol, bRow*3, bl, bh);
			g.drawString("Edit Server Seed", sCol, 170);
			
			g.drawRect(bCol, bRow*4, bl, bh);
			g.drawString("Toggle Harcore", sCol, 220);
			
			g.drawRect(bCol, bRow*5, bl, bh);
			g.drawString("Toggle Online Mode", sCol, 270);
			
			g.drawRect(bCol, bRow*6, bl, bh);
			g.drawString("Toggle PVP", sCol, 320);
			
			g.drawRect(bCol, bRow*7, bl, bh);
			g.drawString("Edit Server Port", sCol, 370);
			
			g.drawRect(bCol, bRow*8, bl, bh);
			g.drawString("Wipe World Content", sCol, 420);
			
			g.drawRect(bCol, bRow*9, bl, bh);
			g.drawString("Visit My Site", sCol, 470);
			
			g.drawRect(bCol, bRow*10, bl, bh);
			g.drawString("Back to Menu", sCol, 520);
			
			//g.drawRect(bCol*5, bRow*1, bl, bh);
			//g.drawString("?", 270, 70);
		}
		else
		{
			//g.drawRect(bCol, bRow*1, bl, bh);
			//g.drawString("Stop Server", sCol, 70);
			
			g.drawRect(bCol, bRow*2, bl, bh);
			g.drawString("Save World", sCol, 120);
			
			g.drawRect(bCol, bRow*3, bl, bh);
			g.drawString("Op Player", sCol, 170);
			
			g.drawRect(bCol, bRow*4, bl, bh);
			g.drawString("De-op Player", sCol, 220);
			
			g.drawRect(bCol, bRow*5, bl, bh);
			g.drawString("List Players Online", sCol, 270);
			
			g.drawRect(bCol, bRow*6, bl, bh);
			g.drawString("Help", sCol, 320);
			
			//g.drawRect(bCol, bRow*7, bl, bh);
			//g.drawString("?", sCol, 370);
			
			g.drawRect(bCol, bRow*8, bl, bh);
			g.drawString("Custom Command", sCol, 420);
			
			g.drawRect(bCol, bRow*9, bl, bh);
			g.drawString("Visit My Site", sCol, 470);
			
			g.drawRect(bCol, bRow*10, bl, bh);
			g.drawString("Stop Server", sCol, 520);
		}

		g.setFont( fnt3 );
		if( running )
		{
			int base = 560;
			for( int i = 0; i < 20; i++ )
			{
				g.setColor( co[i] );
				g.drawString( tx[i], 220, base );
				base -= 30;
			}
		}
		else
		{
			g.setColor(Color.green);
			Map<String, String> m = s.getAllProperties( );
			
			for( int i = 0; i < m.size(); i++ )
			{
				String value = (new ArrayList<String>(m.values())).get(i);
				String key = (new ArrayList<String>(m.keySet())).get(i);
				g.drawString( key + ": " + value,	250, (15+15*i) );			//-250- 420
			}
		}
		
		String t;
		if( s.getSetting( "tekkit" ).contains( "true" ) )
		{
			t = "Tekkit";
		}
		else
		{
			t = "Vanilla";
		}
		
		g.setColor(Color.green);
		g.drawString( t, 180, 45 );
	}
	
	public void tick( ){}
}