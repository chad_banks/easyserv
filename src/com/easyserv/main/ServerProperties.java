package com.easyserv.main;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ServerProperties
{
	/**
	* ServerProperties
	* EasyServ Server Properties Class
	*Mohamad was here.
	* @author  Chad R. Banks
	* @release 3
	* @since   2015-08-18
	*/
	String fileLocation;
	Map<String, String> props = new LinkedHashMap<String, String>();
	
	public ServerProperties( String l )
	{
		fileLocation = l;
		
		try
		{
			String content = new String(Files.readAllBytes( Paths.get( fileLocation ) ));
			String strCollection[] = content.split("\n");
			
			for(int i = 0; i < strCollection.length; i++ )
			{
				String line[] = strCollection[i].split("=");
				
				for(int j = 0; j < line.length; j++ )
				{
					if(j == 1)
					{
						props.put( line[0], line[1] );
					}
				}
			}
		}
		catch(IOException io)
		{
			System.err.println("Error opening server properties for reading.");
			//io.printStackTrace();
		}
	}
	
	public String get( String key )
	{
		return props.get( key );
	}
	
	public Map<String, String> getAll( )
	{
		return props;
	}
	
	public void saveFile( )
	{
		String serverProp = "";
		
		try
		{
			String content = new String(Files.readAllBytes( Paths.get( fileLocation ) ));
			String strCollection[] = content.split("\n");
			
			for(int i = 0; i < strCollection.length; i++ )
			{
				String line[] = strCollection[i].split("=");
				
				for(int j = 0; j < line.length; j++ )
				{
					if(j == 1)
					{
						line[1] = props.get( line[0] );
						serverProp = serverProp + "=";
					}
					
					serverProp = serverProp + line[j];
				}
				serverProp = serverProp + "\n";
			}
		}
		catch(IOException io)
		{
			System.err.println("Error opening server properties for reading.");
			//io.printStackTrace( );
		}
		
		try
		{
			DataOutputStream ostream = new DataOutputStream( new FileOutputStream( fileLocation ) );
			ostream.writeBytes( serverProp );
			ostream.close( );
		}
		catch(IOException io)
		{
			System.err.println("Error opening server properties for writting.");
		}
		catch(InputMismatchException ime)
		{
			System.err.println("Invalid server properties data entry.");
		}
	}
	
	public void toggle( String key )
	{
		if( props.get( key ) == "true" )
		{
			props.put( key, "false" );
		}
		else
		{
			props.put( key, "true" );
		}
		
		saveFile(  );
	}
		
	public void update( String key, String val )
	{
		props.put( key, val );
		
		saveFile( );
	}
}
