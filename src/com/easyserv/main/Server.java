package com.easyserv.main;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.swing.JOptionPane;

public class Server implements Runnable
{
	/**
	* Server
	* EasyServ Server Class
	*
	* @author  Chad R. Banks
	* @release 3
	* @since   2015-08-18
	*/
	BufferedWriter processInput;
	ServerProperties sp;
	ServerProperties ss;
	screenSlot scrn;
	Process proc;
	int slot;
	
	private Thread thread;

	String mc_latest = "V_188";
	String tk_latest = "T_129";
	
	public Server( screenSlot srn, int slt )
	{
		scrn = srn;
		slot = slt;
		ss = new ServerProperties( "EasyServ.properties" );
		
		String built = ss.get( "built" + slot );
		if( built.contains( "false" ) )
		{
			String temDir;
			String tekkit = ss.get( "tekkit" + slot );
			
			if( tekkit.contains( "false" ) )
			{
				temDir = mc_latest;
			}
			else
			{
				temDir = tk_latest;
			}
			
			try
			{
				File template = new File( "assets/server_templates/" + temDir );
				File newSlot = new File( "srv" + slot + "/" );
				FileUtilities.copy( template, newSlot );
				ss.toggle( "built" + slot );
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			
			String downLink;
			String downFile;
			if( tekkit.contains( "false" ) )
			{
				downLink = "https://s3.amazonaws.com/Minecraft.Download/versions/1.8.8/minecraft_server.1.8.8.jar";
				downFile = "minecraft_server.jar";
			}
			else
			{
				downLink = "https://s3.amazonaws.com/Minecraft.Download/versions/1.6.4/minecraft_server.1.6.4.jar";
				downFile = "minecraft_server.1.6.4.jar";
			}
			
			try
			{
				Server.saveUrl( "srv" + slot + "/" + downFile, downLink );
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			//System.out.println( "Already Built!" );
		}
		
		sp = new ServerProperties( "srv" + slot + "/server.properties" );
	}
	
	public static void saveUrl(final String filename, final String urlString) throws MalformedURLException, IOException
	{
	    BufferedInputStream in = null;
	    FileOutputStream fout = null;
	    try
	    {
	        in = new BufferedInputStream(new URL(urlString).openStream());
	        fout = new FileOutputStream(filename);

	        final byte data[] = new byte[1024];
	        int count;
	        while( ( count = in.read( data, 0, 1024 ) ) != -1 )
	        {
	            fout.write( data, 0, count );
	        }
	    }
	    finally
	    {
	        if(in != null)
	        {
	            in.close();
	        }
	        if(fout != null)
	        {
	            fout.close();
	        }
	    }
	}
	
	public Map<String, String> getAllProperties()
	{
		return sp.getAll();
	}
	
	public String getProperty( String str )
	{
		return sp.get( str );
	}
	
	public String getSetting( String str )
	{
		return ss.get( str + slot );
	}
	
	public void toggleProperty( String str )
	{
		sp.toggle( str );
	}
	
	public void toggleSetting( String str )
	{
		ss.toggle( str + slot );
	}
	
	public void purgeDirectory(File dir)
	{
	    for( File file: dir.listFiles( ) )
	    {
	        if( file.isDirectory( ) )
	        	purgeDirectory( file );
	        
	        file.delete( );
	    }
	}
	
	public void updateProperty( String str )
	{
		String con = "";
		if( str.contains( "level-seed" ) )
			con = "server seed.";
		else if( str.contains( "motd" ) )
			con = "server name.";
		
		String userInput = JOptionPane.showInputDialog("Please input your desired " + con);
		
		if( userInput != null )
		{
			sp.update( str, userInput );
		}
	}

	public synchronized void start( )
	{
		thread = new Thread( this );
		thread.start( );
		scrn.out( "Starting...." );
	}
	
	public void op( String plyr )
	{
		try
		{
			scrn.out( "op " + plyr, Color.white );
			processInput.write("op " + plyr + "\r\n");
	        processInput.flush( );
		}
		catch( IOException e )
		{
			e.printStackTrace( );
		}
		catch( Exception e )
		{
			e.printStackTrace( );
		}

	}
	
	public void deop( String plyr )
	{
		try
		{
			scrn.out( "deop " + plyr, Color.white );
			processInput.write("deop " + plyr + "\r\n");
	        processInput.flush();
		}
		catch( IOException e )
		{
			e.printStackTrace( );
		}
		catch( Exception e )
		{
			e.printStackTrace( );
		}

	}
	
	public void comm( String c )
	{
		try
		{
			scrn.out( c, Color.white );
			processInput.write( c + "\r\n" );
	        processInput.flush( );
		}
		catch( IOException e )
		{
			e.printStackTrace( );
		}
		catch( Exception e )
		{
			e.printStackTrace( );
		}

	}
	
	public void stop( )
	{
		try
		{
			scrn.out( "stop", Color.white );
			processInput.write( "stop\r\n" );
	        processInput.flush( );
	        
	        thread.join( );
	        scrn.out( "Done." );
		}
		catch( IOException e )
		{
			e.printStackTrace( );
		}
		catch( Exception e )
		{
			e.printStackTrace( );
		}
	}

	@Override
	public void run( )
	{
		try
		{
			File dir;
			String slt = "srv" + slot;
			String comm1 = "java -version";
			
			dir = new File( slt );
			
			boolean tekkit = true;
			
			if( slot <= 3 )
			{
				tekkit = false;
			}
			
			if( !tekkit )
			{
				comm1 = "java -Xmx2048M -Xms1024M -jar minecraft_server.jar nogui";
			}
			else
			{
				comm1 = "java -Xmx2G -Xms1G -jar Tekkit.jar nogui";
			}
			
			proc = Runtime.getRuntime().exec( comm1, null, dir );

			processInput = new BufferedWriter(new OutputStreamWriter(proc.getOutputStream()));
			
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			BufferedReader stdError;
			
			String str = null;
			
			if( tekkit )
			{
				stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
				while((str = stdError.readLine()) != null)
				{
					Color c = Color.white;
					
					if( str.contains( "WARN" ) )
						c = Color.yellow;
					else if( str.contains( "SEVERE" ) )
						c = Color.red;
					else if( str.contains( "INFO" ) )
						c = Color.green;
					
					scrn.out( str, c );
				}
			}
			else
			{
				while((str = stdInput.readLine()) != null)
				{
					Color c = Color.white;
					
					if( str.contains( "WARN" ) )
						c = Color.yellow;
					else if( str.contains( "SEVERE" ) )
						c = Color.red;
					else if( str.contains( "INFO" ) )
						c = Color.green;
					
					scrn.out( str, c );
				}
			}
		}
		catch( IOException e )
		{
			e.printStackTrace( );
		}
	}
}