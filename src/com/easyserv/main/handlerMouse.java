package com.easyserv.main;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class handlerMouse extends MouseAdapter
{
	/**
	* handlerMouse
	* Listens for mouse clicks and sends them to the view classes (Controller)
	*
	* @author  Chad R. Banks
	* @release 3
	* @since   2015-08-18
	*/
	int x, y;
	private Engine eng;
	
	public handlerMouse( Engine engn )
	{
		eng = engn;
	}
	
	public void mouseReleased( MouseEvent e ){}
	public void mousePressed( MouseEvent e )
	{
		x = e.getX();
		y = e.getY();
		
		if( eng.screenState == StateScreen.Main )
		{
			eng.sMain.onClick( x, y );
		}
		else if( eng.screenState == StateScreen.Slot )
		{
			eng.sSlot.onClick( x, y );
		}
	}
}
