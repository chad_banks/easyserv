package com.easyserv.main;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Window extends Canvas
{
	/**
	* Window
	* Window Class
	*
	* @author  Chad R. Banks
	* @release 3
	* @since   2015-08-18
	* @thanks  RealTutsGML
	*/
	private static final long serialVersionUID = -7259108873705494293L;

	public Window( int w, int h, String title, Engine e )
	{
		JFrame frame = new JFrame( title );
		
		frame.setPreferredSize( new Dimension(w,h) );
		frame.setMaximumSize( new Dimension(w,h) );
		frame.setMinimumSize( new Dimension(w,h) );
		
		frame.setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );
		frame.setResizable( false );
		frame.setLocationRelativeTo( null );
		frame.add( e );
		frame.setVisible( true );
		
		e.start();
	}
}
