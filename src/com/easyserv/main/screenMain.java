package com.easyserv.main;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JOptionPane;

public class screenMain
{
	/**
	* screenMain
	* Main EasyServ Screen (View)
	*
	* @author  Chad R. Banks
	* @release 3
	* @since   2015-08-18
	*/
	private Engine eng;
	static int bCol = 50, bRow = 50, bl = 160, bh = 32;
	static int sCol = 60, sRow = 50;
	
	String motd = "Please give feedback to help me grow!";
	String a = "By clicking the Yes button below, you are indicating your agreement to the Minecraft EULA\n(https://account.mojang.com/documents/minecraft_eula).\n You are also accepting the EasyServ license and terms of use (located in the provided LICENSE.txt file).";
	
	
	public screenMain( Engine engn )
	{
		eng = engn;
		ButtersAPI api = new ButtersAPI( "http://chadrbanks.com/easyserv", "getMOTD" );
		motd = api.getResponse( );
	}
	
	public static boolean mouseOver( int mx, int my, int x, int y, int w, int h )
	{
		if( mx > x && mx < x+w )
		{
			if( my > y && my < y+h )
			{
				return true;
			}
		}
		return false;
	}
	
	public void onClick( int mx, int my )
	{
		if(mouseOver(mx, my, bCol*1, bRow*1, bl+100, bh))	// Slot 1
		{
			eng.slot = 1;
			
			ServerProperties s = new ServerProperties( "EasyServ.properties" );
			String b = s.get( "built" + eng.slot );
			
			if( b.contains( "false" ) )
			{
				int dialogResult = JOptionPane.showConfirmDialog( null, a, "EULA Formalities", JOptionPane.YES_NO_OPTION );
				if( dialogResult == JOptionPane.YES_OPTION )
				{
					eng.sSlot.start( eng.slot );
				}
			}
			else
			{
				eng.sSlot.start( eng.slot );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*2, bl+100, bh))// Slot 2
		{
			eng.slot = 2;
			
			ServerProperties s = new ServerProperties( "EasyServ.properties" );
			String b = s.get( "built" + eng.slot );

			if( b.contains( "false" ) )
			{
				int dialogResult = JOptionPane.showConfirmDialog( null, a, "EULA Formalities", JOptionPane.YES_NO_OPTION );
				if( dialogResult == JOptionPane.YES_OPTION )
				{
					eng.sSlot.start( eng.slot );
				}
			}
			else
			{
				eng.sSlot.start( eng.slot );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*3, bl+100, bh))// Slot 3
		{
			eng.slot = 3;
			
			ServerProperties s = new ServerProperties( "EasyServ.properties" );
			String b = s.get( "built" + eng.slot );
			
			if( b.contains( "false" ) )
			{
				int dialogResult = JOptionPane.showConfirmDialog( null, a, "EULA Formalities", JOptionPane.YES_NO_OPTION );
				if( dialogResult == JOptionPane.YES_OPTION )
				{
					eng.sSlot.start( eng.slot );
				}
			}
			else
			{
				eng.sSlot.start( eng.slot );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*4, bl+100, bh))// Slot 4
		{
			eng.slot = 4;
			
			ServerProperties s = new ServerProperties( "EasyServ.properties" );
			String b = s.get( "built" + eng.slot );
			
			if( b.contains( "false" ) )
			{
				int dialogResult = JOptionPane.showConfirmDialog( null, a, "EULA Formalities", JOptionPane.YES_NO_OPTION );
				if( dialogResult == JOptionPane.YES_OPTION )
				{
					eng.sSlot.start( eng.slot );
				}
			}
			else
			{
				eng.sSlot.start( eng.slot );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*5, bl+100, bh))// Slot 5
		{
			eng.slot = 5;
			
			ServerProperties s = new ServerProperties( "EasyServ.properties" );
			String b = s.get( "built" + eng.slot );
			
			if( b.contains( "false" ) )
			{
				int dialogResult = JOptionPane.showConfirmDialog( null, a, "EULA Formalities", JOptionPane.YES_NO_OPTION );
				if( dialogResult == JOptionPane.YES_OPTION )
				{
					eng.sSlot.start( eng.slot );
				}
			}
			else
			{
				eng.sSlot.start( eng.slot );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*6, bl+100, bh))// Slot 6
		{
			eng.slot = 6;
			
			ServerProperties s = new ServerProperties( "EasyServ.properties" );
			String b = s.get( "built" + eng.slot );
			
			if( b.contains( "false" ) )
			{
				int dialogResult = JOptionPane.showConfirmDialog( null, a, "EULA Formalities", JOptionPane.YES_NO_OPTION );
				if( dialogResult == JOptionPane.YES_OPTION )
				{
					eng.sSlot.start( eng.slot );
				}
			}
			else
			{
				eng.sSlot.start( eng.slot );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*7, bl+100, bh))// Slot 7
		{
			eng.slot = 7;
			
			ServerProperties s = new ServerProperties( "EasyServ.properties" );
			String b = s.get( "built" + eng.slot );
			
			if( b.contains( "false" ) )
			{
				int dialogResult = JOptionPane.showConfirmDialog( null, a, "EULA Formalities", JOptionPane.YES_NO_OPTION );
				if( dialogResult == JOptionPane.YES_OPTION )
				{
					eng.sSlot.start( eng.slot );
				}
			}
			else
			{
				eng.sSlot.start( eng.slot );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*8, bl+100, bh))// Slot 8
		{
			eng.slot = 8;
			
			ServerProperties s = new ServerProperties( "EasyServ.properties" );
			String b = s.get( "built" + eng.slot );
			
			if( b.contains( "false" ) )
			{
				int dialogResult = JOptionPane.showConfirmDialog( null, a, "EULA Formalities", JOptionPane.YES_NO_OPTION );
				if( dialogResult == JOptionPane.YES_OPTION )
				{
					eng.sSlot.start( eng.slot );
				}
			}
			else
			{
				eng.sSlot.start( eng.slot );
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*9, bl, bh))// 9 - Visit Site
		{
			String site = "http://www.chadrbanks.com/easyserv";
			if( Desktop.isDesktopSupported( ) )
			{
			  try
			  {
				  Desktop.getDesktop( ).browse( new URI( site ) );
			  }
			  catch( IOException | URISyntaxException e1 )
			  {
				  e1.printStackTrace( );
			  }
			}
		}
		else if(mouseOver(mx, my, bCol*1, bRow*10, bl, bh))// 10 - Quit
		{
			System.exit( 1 );
		}
	}
	
	public void render( Graphics g )
	{
		Font fnt = new Font( "Arial", 1, 25 );
		Font fnt2 = new Font( "Arial", 1, 15 );
		
		g.setFont( fnt );
		g.setColor(Color.red);
		//g.drawString("EasyServ", 20, 30);

		g.setColor(Color.green);
		g.drawString( "MOTD:", sCol*6, 70 );
		
		g.setFont(fnt2);
		g.drawString( motd, sCol*6, 120 );

		g.setColor(Color.red);
		g.drawRect(bCol, bRow*1, bl+100, bh);
		g.drawString("Slot 1, Vanilla [MC v1.8.8]", sCol, 70);
		
		g.drawRect(bCol, bRow*2, bl+100, bh);
		g.drawString("Slot 2, Vanilla [MC v1.8.8]", sCol, 120);
		
		g.drawRect(bCol, bRow*3, bl+100, bh);
		g.drawString("Slot 3, Vanilla [MC v1.8.8]", sCol, 170);
		
		g.drawRect(bCol, bRow*4, bl+100, bh);
		g.drawString("Slot 4, Vanilla [MC v1.8.8]", sCol, 220);
		
		g.drawRect(bCol, bRow*5, bl+100, bh);
		g.drawString("Slot 5, Tekkit [MC v1.6.5] [T v1.2.9]", sCol, 270);
		
		g.drawRect(bCol, bRow*6, bl+100, bh);
		g.drawString("Slot 6, Tekkit [MC v1.6.5] [T v1.2.9]", sCol, 320);
		
		g.drawRect(bCol, bRow*7, bl+100, bh);
		g.drawString("Slot 7, Tekkit [MC v1.6.5] [T v1.2.9]", sCol, 370);
		
		g.drawRect(bCol, bRow*8, bl+100, bh);
		g.drawString("Slot 8, Tekkit [MC v1.6.5] [T v1.2.9]", sCol, 420);
		
		g.drawRect(bCol, bRow*9, bl, bh);
		g.drawString("Visit My Site", sCol, 470);
		
		g.drawRect(bCol, bRow*10, bl, bh);
		g.drawString("Quit", sCol, 520);
		
		//

		g.setColor(Color.green);
		g.drawString( "Thank you so much for using EasyServ!", 220, 470 );
		g.drawString( "Click the site button to the left to give me feedback.", 220, 500 );
		g.drawString( "I would love to hear from you!", 220, 530 );
		g.drawString( "- Chad", 220, 560 );
	}
	
	public void tick( ){}
}
