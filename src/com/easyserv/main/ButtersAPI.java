package com.easyserv.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

public class ButtersAPI
{
	/**
	* ButtersAPI
	* API for pulling data, such as the motd, from my server
	*
	* @author  Chad R. Banks
	* @release 3
	* @since   2015-08-18
	*/
	private String response = "";
	
	public ButtersAPI( String u, String api )
	{
		try
		{
			URL url = new URL(u);
	        Map<String,Object> params = new LinkedHashMap<>();
	        params.put( "r", Engine.release );
	        params.put( "zAjax", "true" );		// Deprecated in latest server version
	        params.put( "api", api );

	        StringBuilder postData = new StringBuilder();
	        for( Map.Entry<String,Object> param : params.entrySet() )
	        {
	            if(postData.length() != 0) postData.append('&');
	            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	            postData.append('=');
	            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
	        }
	        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	        conn.setDoOutput(true);
	        conn.getOutputStream().write(postDataBytes);

	        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

	        for ( int c = in.read(); c != -1; c = in.read() )
            {
	        	response = response + (char)c;
            }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public String getResponse()
	{
		return response;
	}
}
